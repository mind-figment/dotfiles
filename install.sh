#!/bin/bash

########
# nvim #
########

mkdir -p "$XDG_CONFIG_HOME/nvim"
mkdir -p "$XDG_CONFIG_HOME/nvim/undo"
ln -sf "$DOTFILES/nvim/init.vim" "$XDG_CONFIG_HOME/nvim"

# install neovim plugin manager
[ ! -f "$DOTFILES/nvim/autoload/plug.vim" ] \
    && curl -fLo "$DOTFILES/nvim/autoload/plug.vim" --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

mkdir -p "$XDG_CONFIG_HOME/nvim/autoload"
ln -sf "$DOTFILES/nvim/autoload/plug.vim" "$XDG_CONFIG_HOME/nvim/autoload"

# Install (or update) all the plugins
nvim --noplugin +PlugUpdate +qa

#######
# X11 #
#######

rm -rf "$XDG_CONFIG_HOME/X11"
ln -s "$DOTFILES/X11" "$XDG_CONFIG_HOME"

#######
# ZSH #
#######

mkdir -p "$XDG_CONFIG_HOME/zsh"
ln -sf "$DOTFILES/zsh/.zshenv" "$HOME"
ln -sf "$DOTFILES/zsh/.zshrc" "$XDG_CONFIG_HOME/zsh"
ln -sf "$DOTFILES/zsh/aliases" "$XDG_CONFIG_HOME/zsh"

rm -rf "$XDG_CONFIG_HOME/zsh/external"
ln -sf "$DOTFILES/zsh/external" "$XDG_CONFIG_HOME/zsh"

#######
# GIT #
#######

mkdir -p "$XDG_CONFIG_HOME/git"
ln -sf "$DOTFILES/git/.gitconfig" "$HOME" #"$XDG_CONFIG_HOME/git"

#########
# QTILE #
#########

rm -rf "$XDG_CONFIG_HOME/qtile"
ln -s "$DOTFILES/qtile" "$XDG_CONFIG_HOME"

########
# TMUX #
########

mkdir -p "$XDG_CONFIG_HOME/tmux"
ln -sf "$DOTFILES/tmux/tmux.conf" "$XDG_CONFIG_HOME/tmux"

[ ! -d "$XDG_CONFIG_HOME/tmux/plugins/tpm" ] \
    && git clone https://github.com/tmux-plugins/tpm \
    "$XDG_CONFIG_HOME/tmux/plugins/tpm"

mkdir -p "$XDG_CONFIG_HOME/tmuxp"
ln -sf "$DOTFILES/tmuxp/dotfiles.yml" "$XDG_CONFIG_HOME/tmuxp"

#########
# PICOM #
#########

mkdir -p "$XDG_CONFIG_HOME/picom"
ln -sf "$DOTFILES/picom/picom.conf" "$XDG_CONFIG_HOME/picom"

###########
# ZATHURA #
###########

mkdir -p "$XDG_CONFIG_HOME/zathura"
ln -sf "$DOTFILES/zathura/zathurarc" "$XDG_CONFIG_HOME/zathura"

#############
# ALACRITTY #
#############

mkdir -p "$XDG_CONFIG_HOME/alacritty"
ln -sf "$DOTFILES/alacritty/alacritty.yml" "$XDG_CONFIG_HOME/alacritty"
rm -rf "$XDG_CONFIG_HOME/alacritty/colors"
ln -s "$DOTFILES/alacritty/colors"  "$XDG_CONFIG_HOME/alacritty"

############
# NITROGEN #
############

mkdir -p "$XDG_CONFIG_HOME/nitrogen"
ln -sf "$DOTFILES/nitrogen/bg-saved.cfg" "$XDG_CONFIG_HOME/nitrogen"
ln -sf "$DOTFILES/nitrogen/nitrogen.cfg" "$XDG_CONFIG_HOME/nitrogen"

############
# STARSHIP #
############

mkdir -p "$XDG_CONFIG_HOME/starship"
ln -sf "$DOTFILES/starship/starship.toml" "$XDG_CONFIG_HOME/starship"

########
# HTOP #
########

mkdir -p "$XDG_CONFIG_HOME/htop"
ln -sf "$DOTFILES/htop/htoprc" "$XDG_CONFIG_HOME/htop"

########
# VIFM #
########

rm -rf "$XDG_CONFIG_HOME/vifm"
ls -s "$DOTFILES/vifm" "$XDG_CONFIG_HOME"

