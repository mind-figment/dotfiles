#!/usr/bin/env bash

redshift -P -O 4000 &
nitrogen --restore &
picom &
setxkbmap -option caps:escape
setxkbmap -option 'caps:ctrl_modifier';xcape -e 'Caps_Lock=Escape' &
